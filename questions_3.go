package main

import (
    "testing"
    "reflect"
)

func MostFrequent(arr []string) []string {
	arrNew := []string{}
        var i int
	characterNumberSequence := make([]int,len(arr))

	for i = 0; i < len(arr); i++ {
		dupcount := 1
		for j := i + 1; j < len(arr); j++ {
			if arr[i] == arr[j] {

				dupcount++
				characterNumberSequence[i] = dupcount

			}
		}
	}
	if(len(characterNumberSequence)==0){
	 return arrNew
	}
	max :=characterNumberSequence[0]
	maxIndis := 0

	for i := 1; i < len(characterNumberSequence); i++ {
		if max < characterNumberSequence[i] {
		
			max = characterNumberSequence[i]
			maxIndis= i
			
		}
	}
	
	var limit []int
	for i := 0; i< len(characterNumberSequence); i++ {
		if max == characterNumberSequence[i] {
			
			limit = append(limit,i)	
		}
	}
	if len(limit) == 1 {
		arrNew = append(arrNew,arr[maxIndis])
		
	}
	
	if len(limit) > 1 {
	   
		for i :=0; i<len(limit); i++ {
		    arrNew = append(arrNew,arr[limit[i]])
		    
			
		}
		 
	}
    return arrNew
}
// testing
func TestMostFrequentBasic(t *testing.T) {
    girilenDizi := []string{"bluee","aa","aa"}
    sonuc :=[]string{"aa"}
    ans := MostFrequent(girilenDizi)
    if reflect.DeepEqual(ans, sonuc) !=true {
	
      t.Errorf("Error Function")
   }
}
//
func TestMostFrequentEqual(t *testing.T) {
    girilenDizi := []string{"bluee","aa","aa","bluee"}
    sonuc :=[]string{"bluee","aa"}
    ans := MostFrequent(girilenDizi)
    if reflect.DeepEqual(ans, sonuc) !=true {
      t.Errorf("Error Function")
   }
}
func TestMostFrequentFree(t *testing.T) {
    girilenDizi := []string{}
    sonuc :=[]string{}
    ans := MostFrequent(girilenDizi)
    if reflect.DeepEqual(ans, sonuc) !=true {
      t.Errorf("Error Function")
   }
}
