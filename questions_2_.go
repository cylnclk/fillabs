package main

import (
        "fmt"
        "math"
)

// It is an algorithm that finds the numbers starting from 2 up to n and going out of the square root.
// Ex. for 9 = 2 4 9 
//Ex2 for 36 = 2 4 9 16 25 36 
//Ex3 for 81 =2 4 9 16 25 36 49 64 81


func FindSquare(param float64) float64 {
	fmt.Println(param)
	outSquare := math.Sqrt(param)
	temp := outSquare-1 
	
	if temp >1  {
		return FindSquare(temp*temp)
	}else {
		return outSquare
	}

}


func main() {
	 //fmt.Println(FindSquare(81))
	//fmt.Println(FindSquare(36))
	fmt.Println(FindSquare(9))
}
