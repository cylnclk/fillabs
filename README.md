
# In this Project, there are questions and answers given in the interview of Fillabs company.

### There are 3 questions and 1 crud in the project.

**Q1)** Write a function that sorts a bunch of words by the number of character “a”s within the
word (decreasing order). If some words contain the same amount of character “a”s then you
need to sort those words by their lengths.
 

**Input :**
["aaaasd", "a", "aab", "aaabcd", "ef", "cssssssd", "fdz", "kf", "zc", "lklklklklklklklkl", "l"]

**Output :**
["aaaasd", "aaabcd", "aab", "a", "lklklklklklklklkl", "cssssssd", "fdz", "ef", "kf", "zc", "l"]

The answer to the problem is in the **question_1 **file.

**Q2)** Write a recursive function which takes one integer parameter. Please bear in mind that
finding the algorithm needed to generate the output below is the main point of the question.
Please do not ask which algorithm to use.

**Input :**
9

**Output :**
2
4
9

**Q3)** Write a function which takes one parameter as an array/list. Find most repeated data
within a given array.
Test with different datasets.

**Input :**
["apple","pie","apple","red","red","red"]

**Output :**
"red" 

The answer to the problem is in the **question_3** file.


To see the output of the questions:

 **go run dosya_adi.go**   

run the command.
 <hr>

 **Q4)** Write a user management project which will include;


- A master view which will list all users in a data grid. This screen will assist users with all
CRUD operations. User will be able to press 3 buttons
(New,Edit,Delete). Edit and Delete operations will require row selection from the data
grid.
-  A detailed view which will show the fields as form. Form will have 2 buttons (Action, Back).
Text of the action button will change according to the
operation opened the detail view. For example if the “New” operation is selected from the
master, the detailed view action button text will be “Create”. Please see the mappings
below.

- New: Create
- Edit: Save
- Delete: Delete
- A REST service to support functions below. Please note that API paths and HTTP methods and HTTP
Statuses are important for us.
- getAll: Returns all users
- get/{id}: Return the user with the desired “id”
- create: Save the given user.
- update/{id}: Update data of the user with the desired “id”
- delete/{id}: Delete the user with the desired “id”
- Backend must be written with Go. You are free to choose any
database you desire. Remember all operations must be persistent.
- Frontend must be written with JS/TS using React.
