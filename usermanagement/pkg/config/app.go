package config

import (
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
)

var (
	db *gorm.DB
)

func Connect() {
	//con, err := sql.Open("mysql", root+":"++"@/"+book)
	//defer con.Close()
	d, err := gorm.Open("mysql", "root:@(127.0.0.1:3306)/usermanagement")
	if err != nil {
		panic(err)
	}
	db = d
}
func GetDB() *gorm.DB {
	return db
}
