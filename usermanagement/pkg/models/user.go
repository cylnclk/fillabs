package models

import (
	"github.com/akhil/go-bookstore/pkg/config"
	"github.com/jinzhu/gorm"
)

var dbu *gorm.DB

type User struct {
	gorm.Model
	Name     string `gorm:"json:"name"`
	Surname  string `json:"surname"`
	Email    string `json:"email" `
	Password string `json:"password" `
}

func init() {
	config.Connect()
	dbu = config.GetDB()
	dbu.AutoMigrate(&User{})
}

func (b *User) CreateUser() *User {
	dbu.NewRecord(b)
	dbu.Create(&b)
	return b
}

func GetAllUsers() []User {
	var Users []User
	dbu.Find(&Users)
	return Users
}
func GetUserById(Id int64) (*User, *gorm.DB) {
	var getUser User
	dbu := dbu.Where("ID=?", Id).Find(&getUser)
	return &getUser, dbu
}

func DeleteUser(ID int64) User {
	var user User
	dbu.Where("ID=?", ID).Delete(user)
	return user
}
